const{test,expect}=require('@playwright/test')

test('sub',async({page}) =>{

await page.goto('https://testing.kriyadocs.com/submission/kriya/kriya')
//to select special issue button
await page.locator('#specialIssue').click()

//to select and fill the article title
await page.locator('summarynote').fill('hyyyyy')

//to select and fill the Abstract
await page.locator('.col-12 articleTitleGroup').fill('helloo')

//to select keyword textbox
await page.locator('.keywordDiv keywordObject row m-0 align-items-center').click()

// to fill keyword textbox
await page.locator('.input-field').fill('hyyy')

//to click the save button
await page.getByRole('button' ,{name:'Save'}).click()


})