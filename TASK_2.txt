TASK 2

CSS SELECTOR FOR GIVEN ELEMENT

* search by id ,articlename element
     page.getbyRole('textbox',(name:'small');

*to get filter element
   page.getbyRole('button',(name:'filters');
               (or)
   page.locator('text=Filters ');

*to get add article element
   page.getbyRole('button',(name:'Add article');
                (or)
   page.locator('text=Add article')